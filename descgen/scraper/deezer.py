#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 tycho
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import itertools

from .base import GetReleaseResultMixin, GetListResultMixin, ExceptionMixin, RequestMixin, StandardFactory
from .base import Scraper as ScraperBase
from .base import SearchScraper as SearchScraperBase
from ..result import NotFoundResult

READABLE_NAME = 'Deezer'
SCRAPER_URL = 'https://www.deezer.com/'
NOTES = u'Featured and Remix artists are not supported.'

_API_BASE_URL = 'https://api.deezer.com/'
RATE_LIMIT = '5/s'
CONCURRENCY = 1

class Scraper(GetReleaseResultMixin, ScraperBase, RequestMixin, ExceptionMixin):

    _base_url = SCRAPER_URL
    _api_base_url = _API_BASE_URL

    string_regex = r'^http(?:s)?://(?:www\.)?deezer\.com/(?:[a-z]{2}/)?album/(\d+)(?:/)?$'

    VARIOUS_ARTISTS_ALIASES = ['various artists']

    @staticmethod
    def _get_args_from_match(match):
        return (int(match.group(1)), )

    def __init__(self, id):
        super(Scraper, self).__init__()
        self.id = id

        self._disc_containers = None

        self._release_artists_equal_track_artists = True

    def get_instance_info(self):
        return u'id="%d"' % self.id

    def get_url(self):
        return self._base_url + 'album/%d' % self.id

    def get_api_url(self):
        return self._api_base_url + 'album/%d' % self.id

    def get_initial_data(self):
        # for some reason the client certificate cannot be verified (most likely because its CA is not in the store
        # used by python) - as we don't care much about the security of this connection we just disable verification
        # for now
        response = self.request_get(url=self.get_api_url())
        return self.get_response_content(response)

    def process_initial_data(self, initial_data):
        try:
            response = json.loads(initial_data)
            if 'error' in response:
                self.result = self.instantiate_result(NotFoundResult)
        except ValueError:
            self.raise_exception(u'invalid server response: %r' % initial_data)
        return response

    def add_release_event(self):
        if 'release_date' in self.data:
            release_event = self.result.create_release_event()
            release_event.set_date(self.data['release_date'])
            self.result.append_release_event(release_event)

    def add_label_ids(self):
        if 'label' in self.data:
            label_id = self.result.create_label_id()
            label_id.set_label(self.data['label'])
            self.result.append_label_id(label_id)

    def add_release_title(self):
        if 'title' in self.data:
            self.result.set_title(self.data['title'])

    def add_release_artists(self):
        if 'artist' in self.data:
            artist_name = self.data['artist']['name']
            artist = self.result.create_artist()
            if artist_name.lower() in self.VARIOUS_ARTISTS_ALIASES:
                artist.set_various(True)
            else:
                artist.set_name(artist_name)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)

            self._check_if_release_artist_equals_track_artists()

    def add_genres(self):
        if 'genres' in self.data:
            for genre in [item['name'] for item in self.data['genres']['data']]:
                self.result.append_genre(genre)

    def _group_tracks_by_disc(self, tracks):
        discs = {}
        for track in tracks:
            disc_number = track['disk_number']
            if disc_number not in discs:
                discs[disc_number] = []
            discs[disc_number].append(track)

        return discs

    def _get_tracklist(self, tracklist_url):
        response = self.request_get(url=tracklist_url)
        content = self.process_initial_data(self.get_response_content(response))
        tracklist = content['data']
        if 'next' in content:
            tracklist += self._get_tracklist(content['next'])
        return tracklist

    def get_disc_containers(self):
        if not self._disc_containers:
            self._disc_containers = {}
            if 'tracklist' in self.data:
                tracklist = self._get_tracklist(self.data['tracklist'])
                self._disc_containers = self._group_tracks_by_disc(tracklist)
        return self._disc_containers

    def get_track_number(self, track_container):
        if 'track_position' in track_container:
            return track_container['track_position']

    def get_track_title(self, track_container):
        if 'title' in track_container:
            return track_container['title']

    def get_track_length(self, track_container):
        if 'duration' in track_container:
            return track_container['duration']

    def _check_if_release_artist_equals_track_artists(self):
        release_artists = sorted(self.result.get_release_artists())
        discs = self.get_disc_containers()
        for track_container in itertools.chain(*discs.values()):
            track_artists = sorted(self._get_track_artists(track_container))
            self._release_artists_equal_track_artists = release_artists == track_artists
            if not self._release_artists_equal_track_artists:
                break

    def _get_track_artists(self, track_container):
        artists = []
        if 'artist' in track_container:
            artist = self.result.create_artist()
            artist.set_name(track_container['artist']['name'])
            artist.append_type(self.result.ArtistTypes.MAIN)
            artists = [artist]
        return artists

    def get_track_artists(self, track_container):
        if self._release_artists_equal_track_artists:
            return []
        return self._get_track_artists(track_container)

class SearchScraper(GetListResultMixin, SearchScraperBase, RequestMixin):

    url = 'https://api.deezer.com/search/album'

    def get_initial_data(self):
        # for some reason the client certificate cannot be verified (most likely because its CA is not in the store
        # used by python) - as we don't care much about the security of this connection we just disable verification
        # for now
        response = self.request_get(url=self.url, params={'q': self.search_term})
        return self.get_response_content(response)

    def process_initial_data(self, initial_data):
        response = json.loads(initial_data)
        return response

    def get_release_containers(self):
        if 'data' in self.data:
            return self.data['data']
        return []

    def get_release_name(self, release_container):
        artist = release_container['artist']['name']
        title = release_container['title']
        return artist + u' \u2013 ' + title

    def get_release_url(self, release_container):
        return release_container['link']


class ScraperFactory(StandardFactory):
    scraper_classes = (Scraper, )
    search_scraper = SearchScraper

    global_rate_limit = RATE_LIMIT
    global_concurrency = CONCURRENCY
