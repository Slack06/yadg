#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json

from lxml import html

from .base import Scraper, ExceptionMixin, RequestMixin, UtilityMixin, StandardFactory, GetReleaseResultMixin, GetListResultMixin
from .base import SearchScraper as SearchScraperBase
from ..result import NotFoundResult

READABLE_NAME = 'iTunes Store'
SCRAPER_URL = 'http://music.apple.com/'
NOTES = 'Multiple track artists or release artists are not split correctly.'


class ReleaseScraper(GetReleaseResultMixin, Scraper, RequestMixin, ExceptionMixin, UtilityMixin):

    _base_url = 'http://music.apple.com/%s/album/'
    string_regex = '^https?://music\.apple\.com/(\w{2,4})/album/([^/]*)/([^\?]+)[^/]*$'
    forced_response_encoding = 'utf8'

    ITEM_KINDS = ['trackLockup']

    EXCLUDE_TAG_MUSIC = ['Music', u'ミュージック']

    def __init__(self, store, release_name, id):
        super(ReleaseScraper, self).__init__()
        self.id = id
        self.store = store
        self._release_name = release_name
        self._release_artists = set()

    def get_instance_info(self):
        return u'id=%s, store=%s' % (self.id, self.store)

    def get_url(self):
        return self._base_url % self.store + self._release_name + '/' + self.id

    def add_release_event(self):
        release_date = self.data.get('release').get('datePublished')
        if release_date:
            release_event = self.result.create_release_event()
            release_event.set_date(release_date)
            self.result.append_release_event(release_event)

    def add_release_title(self):
        release_title = self.data.get('release').get('name')
        if release_title:
            self.result.set_title(release_title)

    def add_release_artists(self):
        release_artists = self.data.get('release').get('byArtist', [])
        if release_artists:
            for release_artist in release_artists:
                artist_name = release_artist.get('name')
                if artist_name:
                    artist = self.result.create_artist()
                    artist.set_name(artist_name)
                    artist.append_type(self.result.ArtistTypes.MAIN)
                    self.result.append_release_artist(artist)
                    self._release_artists.add(artist_name)

        else:
            artist = self.result.create_artist()
            artist.set_various(True)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)

    def add_genres(self):
        genres = self.data.get('release').get('genre', [])
        for name in genres:
            if name not in self.EXCLUDE_TAG_MUSIC:
                self.result.append_genre(name)

    def get_disc_containers(self):
        disc_containers = {}
        discs = self.data.get('discs')
        for disc in discs:
            items = disc.get('items', [])
            for item in items:
                disc_nr = item.get('discNumber')
                if disc_nr not in disc_containers:
                    disc_containers[disc_nr] = []
                disc_containers[disc_nr].append(item)
        return disc_containers

    def get_track_number(self, track_container):
        track_number = track_container.get('trackNumber')
        if track_number:
            return str(track_number)

    def get_track_artists(self, track_container):
        track_artists = []
        artist = track_container.get('artistName')
        artists = track_container.get('subtitleLinks', [])
        all_artists = map(lambda x: x.get('title'), artists) if artists else [artist]
        for name in all_artists:
            if name in self._release_artists:
                continue
            track_artist = self.result.create_artist()
            track_artist.set_name(name)
            track_artist.append_type(self.result.ArtistTypes.MAIN)
            track_artists.append(track_artist)
        return track_artists

    def get_track_title(self, track_container):
        return track_container.get('title')

    def get_track_length(self, track_container):
        track_length = track_container.get('duration', 0)
        return track_length / 1000 if isinstance(track_length, int) else track_length

    def process_initial_data(self, initial_data):
        document = html.document_fromstring(initial_data)
        script = document.cssselect('script#serialized-server-data')
        try:
            raw = json.loads(script[0].text_content())
            data = raw[0].get('data')
            return {
                'release': data.get('seoData', {}).get('schemaContent', {}),
                'discs': filter(lambda d: d.get('itemKind') in self.ITEM_KINDS, data.get('sections', []))
            }
        except (ValueError, IndexError):
            self.raise_exception(u'Could not parse "#serialized-server-data"')


class SearchScraper(GetListResultMixin, SearchScraperBase, RequestMixin, ExceptionMixin):

    url = 'http://itunes.apple.com/search'

    def get_url(self):
        return self.url

    def get_params(self):
        return {'media': 'music', 'entity': 'album', 'limit': '25', 'term': self.search_term}

    def process_initial_data(self, initial_data):
        try:
            response = json.loads(initial_data)
        except ValueError:
            self.raise_exception(u'invalid server response: %s' % initial_data)
        return response

    def get_release_containers(self):
        return self.data['results'] if 'results' in self.data else []

    def get_release_name(self, release_container):
        components = [
            release_container[key]
            for key in ['artistName', 'collectionName']
            if key in release_container
        ]
        return u' \u2013 '.join(components)

    def get_release_info(self, release_container):
        components = []
        release_date = release_container.get('releaseDate')
        if release_date:
            components.append(release_date.split('T')[0])
        components.extend(
            release_container[key]
            for key in ['country', 'primaryGenreName']
            if key in release_container
        )
        return u' | '.join(components)

    def get_release_url(self, release_container):
        release_url = release_container.get('collectionViewUrl')
        return release_url if release_url else None


class ScraperFactory(StandardFactory):

    scraper_classes = [ReleaseScraper]
    search_scraper = SearchScraper
