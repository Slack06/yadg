#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import lxml.html
import re
from .base import Scraper, ExceptionMixin, RequestMixin, UtilityMixin, StatusCodeError, StandardFactory, GetReleaseResultMixin, GetListResultMixin
from .base import SearchScraper as SearchScraperBase
from ..result import NotFoundResult


READABLE_NAME = 'Junodownload'
SCRAPER_URL = 'http://www.junodownload.com/'
NOTES = 'Multiple track artists are not split correctly.'


class ReleaseScraper(GetReleaseResultMixin, Scraper, RequestMixin, ExceptionMixin, UtilityMixin):

    _base_url = 'https://www.junodownload.com/'
    string_regex = '^http(?:s)?://(?:(?:www|secure)\.)?junodownload\.com/products/([^/]*)/([^/]*)/?$'
    request_kwargs = {'allow_redirects': False}

    _various_artists_aliases = ['various']

    def __init__(self, release_name, id):
        super(ReleaseScraper, self).__init__()
        self.id = id
        self._release_name = release_name

    def get_instance_info(self):
        return u'id=%s' % self.id

    def get_url(self):
        return self._base_url + 'products/%s/%s/' % (self._release_name, self.id)

    def process_initial_data(self, initial_data):
        # there is at least one release with an unescaped "<" in the track title
        # try to mitigate the problem, although that might break more than it fixes
        content = re.sub('(?s)(<div[^>]*?class="product_tracklist_heading_records_title"[^>]*?>)(.*?)(?=</div>)',
                         lambda x: x.group(1)+x.group(2).replace('<', '&lt;').replace('>', '&gt;'), initial_data)
        #get the raw response content and parse it
        return lxml.html.document_fromstring(content)

    def add_release_event(self):
        release_date_span = self.data.cssselect('span[itemprop="datePublished"]')
        if len(release_date_span) == 1:
            release_date = release_date_span[0].text_content()
            release_date = self.remove_whitespace(release_date)
            if release_date:
                release_event = self.result.create_release_event()
                release_event.set_date(release_date)
                self.result.append_release_event(release_event)

    def add_label_ids(self):
        cat_nr_strong = self.data.xpath('//strong[text()="Cat:"]')
        cat_nr = None
        if len(cat_nr_strong) == 1:
            cat_nr = cat_nr_strong[0].tail
            cat_nr = self.remove_whitespace(cat_nr)
        label_anchors = self.data.cssselect('.product-label a')
        for label_anchor in label_anchors:
            label_name = label_anchor.text_content()
            label_name = self.remove_whitespace(label_name)
            if label_name:
                label_id = self.result.create_label_id()
                label_id.set_label(label_name)
                if cat_nr:
                    label_id.append_catalogue_nr(cat_nr)
                self.result.append_label_id(label_id)

    def add_release_title(self):
        title_anchor = self.data.cssselect('.product-title a')
        if len(title_anchor) != 1:
            self.raise_exception(u'could not get release title anchor')
        title = title_anchor[0].text_content()
        title = self.remove_whitespace(title)
        if title:
            self.result.set_title(title)

    def add_release_artists(self):
        in_breadcrumbs = True
        artist_anchors = self.data.cssselect('div#topbar_bread div.breadcrumb_text span a')
        artist_anchors = filter(lambda x: x.attrib['href'].startswith('/artists/'), artist_anchors)
        if not artist_anchors:
            #apparently the artist is not always part of the breadcrumbs
            artist_anchors = self.data.cssselect('.product-artist a')
            if artist_anchors:
                artist_anchors = filter(lambda x: x.attrib['href'].startswith('/artists/'), artist_anchors)
            else:
                artist_anchors = self.data.cssselect('.product-artist')
            in_breadcrumbs = False
        is_feature = False
        has_various = False
        for artist_anchor in artist_anchors:
            artist_name = artist_anchor.text_content()
            artist_name = self.remove_whitespace(artist_name)
            if not in_breadcrumbs:
                #do some rudimentary capitalizing if we did not get the artists from the breadcrumbs
                artist_name = u' '.join(map(lambda x: x.capitalize(), artist_name.split()))
            if artist_name.lower() in self._various_artists_aliases:
                has_various = True
            elif artist_name:
                artist = self.result.create_artist()
                artist.set_name(artist_name)
                if is_feature:
                    artist.append_type(self.result.ArtistTypes.FEATURING)
                else:
                    artist.append_type(self.result.ArtistTypes.MAIN)
                self.result.append_release_artist(artist)
            if not is_feature and artist_anchor.tail and 'feat' in artist_anchor.tail:
                # all artists after this one are features
                is_feature = True
        if not self.result.get_release_artists() and has_various:
            # only add the 'Various' to artists if it is the only one
            artist = self.result.create_artist()
            artist.set_various(True)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)

    def add_genres(self):
        genre_anchors = self.data.cssselect('meta[itemprop="genre"]')
        for genre_anchor in genre_anchors:
            genre_string = genre_anchor.get('content')
            genre_strings = genre_string.split('/')
            genre_strings = filter(lambda x: x, map(self.remove_whitespace, genre_strings))
            for genre in genre_strings:
                self.result.append_genre(genre)

    def get_disc_containers(self):
        tracklist_div = self.data.cssselect('.product-tracklist')
        if len(tracklist_div) != 1:
            self.raise_exception(u'could not find tracklist div')
        return {1: tracklist_div[0]}

    def get_track_containers(self, disc_container):
        return disc_container.cssselect('.product-tracklist-track[itemprop="track"]')

    def get_track_number(self, track_container):
        track_title_col = track_container.cssselect('.track-title')
        if len(track_title_col) != 1:
            self.raise_exception(u'could not find track number div')
        track_nr = track_title_col[0].text_content()
        track_nr = self.remove_whitespace(track_nr)
        if track_nr:
            return str(track_nr.split('.', 1)[0])
        return None

    def _get_track_title_element(self, track_container):
        title_div = track_container.cssselect('.track-title')
        if len(title_div) != 1:
            self.raise_exception(u'could not find title div')
        return title_div[0]

    def _get_track_artist(self, track_div):
        artist_meta = track_div.cssselect('meta[itemprop="byArtist"]')
        if not artist_meta:
            return None
        artist = artist_meta[0].get('content')
        return artist

    def get_track_artists(self, track_container):
        title_el = self._get_track_title_element(track_container)
        artist_name = self._get_track_artist(title_el)
        if not artist_name:
            return []
        artist = self.result.create_artist()
        artist.set_name(artist_name)
        artist.append_type(self.result.ArtistTypes.MAIN)
        return [artist]

    def get_track_title(self, track_container):
        title_el = self._get_track_title_element(track_container)
        title_span = title_el.cssselect('a span')
        artist = self._get_track_artist(title_el)
        title = self.remove_whitespace(title_span[0].text_content())
        if artist:
            regex = r'^' + re.escape(artist) + r' - '
            title = re.sub(regex, '', title)
        if title:
            return title
        return None

    def get_track_length(self, track_container):
        columns = track_container.getchildren()
        if len(columns) == 4:
            length = columns[1].text
            length = self.remove_whitespace(length)
            if length:
                return self.seconds_from_string(length)
        return None

    def handle_initial_data_exception(self, exception):
        if isinstance(exception, StatusCodeError):
            # non existent release pages are automatically forwarded to a search
            # make sure we return a NotFoundResult in that case
            if str(exception) == "404" or str(exception) == "302":
                self.result = self.instantiate_result(NotFoundResult)
            else:
                self.raise_exception("request to server unsuccessful, status code: %s" % str(exception))
        else:
            raise exception


class SearchScraper(GetListResultMixin, SearchScraperBase, RequestMixin, ExceptionMixin, UtilityMixin):

    _base_url = 'https://www.junodownload.com'
    url = 'https://www.junodownload.com/search/'

    def get_url(self):
        return self.url

    def get_params(self):
        return {'solrorder': 'relevancy', 'submit-search': 'SEARCH', 'q[all][]': self.search_term}

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def get_release_containers(self):
        return self.data.cssselect('div.jd-listing-item')[:25]

    def get_release_name(self, release_container):
        components = []
        release_artist_div = release_container.cssselect('div.juno-artist')
        if len(release_artist_div) == 1:
            release_artists = release_artist_div[0].text_content()
            release_artists = self.remove_whitespace(release_artists)
            if release_artists:
                components.append(release_artists)
        release_title_div = release_container.cssselect('a.juno-title')
        if len(release_title_div) == 1:
            release_title = release_title_div[0].text_content()
            release_title = self.remove_whitespace(release_title)
            if release_title:
                components.append(release_title)
        if components:
            return u' \u2013 '.join(components)
        self.raise_exception(u'could not determine release name')

    def get_release_info(self, release_container):
        components = []
        label_a = release_container.cssselect('a.juno-label')
        if len(label_a) == 1:
            label = label_a[0].text_content()
            label = self.remove_whitespace(label)
            if label:
                components.append(label)
        additional_info_div = release_container.cssselect('div[data-ua_location="release header"] > div:first-child')
        if len(additional_info_div) == 1:
            additional_info = additional_info_div[0].text
            additional_infos = [additional_info] + map(lambda x: x.tail, additional_info_div[0].getchildren())
            additional_infos = filter(lambda x: x, map(self.remove_whitespace, additional_infos))
            if additional_infos:
                components.extend(additional_infos)
        if components:
            return u' | '.join(components)
        return None

    def get_release_url(self, release_container):
        release_anchor = release_container.cssselect('a.juno-title')
        if len(release_anchor) != 1:
            self.raise_exception(u'could not find release link anchor')
        release_link = self._base_url + release_anchor[0].attrib['href']
        m = re.match(ReleaseScraper.string_regex, release_link)
        if not m:
            release_link = None
        return release_link


class ScraperFactory(StandardFactory):

    scraper_classes = [ReleaseScraper]
    search_scraper = SearchScraper