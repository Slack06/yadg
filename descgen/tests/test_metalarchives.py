#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import TestCase
from ..scraper import metalarchives
from ..result import ReleaseResult, ListResult, NotFoundResult


class MetalarchivesTest(TestCase):

    def test_simple_album(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date('June 24th, 2002')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format(u'Full-length, CD')

        label_id = expected.create_label_id()
        label_id.set_label('Spinefarm Records')
        label_id.append_catalogue_nr('spi149CD / 018 459-2')
        expected.append_label_id(label_id)

        expected.set_title('Century Child')

        artist = expected.create_artist()
        artist.set_name('Nightwish')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.set_url('http://www.metal-archives.com/albums/Nightwish/Century_Child/3719')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Bless the Child')
        track.set_length(372)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title('End of All Hope')
        track.set_length(235)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title('Dead to the World')
        track.set_length(260)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title('Ever Dream')
        track.set_length(284)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title('Slaying the Dreamer')
        track.set_length(272)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title('Forever Yours')
        track.set_length(230)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title('Ocean Soul')
        track.set_length(255)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title('Feel for You')
        track.set_length(235)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title('The Phantom of the Opera')
        track.set_length(250)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title('Beauty of the Beast')
        track.set_length(622)
        disc.append_track(track)

        expected.append_disc(disc)

        s = metalarchives.ReleaseScraper.from_string('http://www.metal-archives.com/albums/Nightwish/Century_Child/3719')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_multiple_cds(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date('September 22nd, 2008')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format(u'Live album, 2CD')

        label_id = expected.create_label_id()
        label_id.set_label('GUN Records')
        label_id.append_catalogue_nr('88697 342672')
        expected.append_label_id(label_id)

        expected.set_title('Black Symphony')

        artist = expected.create_artist()
        artist.set_name('Within Temptation')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.set_url('http://www.metal-archives.com/albums/Within_Temptation/Black_Symphony/212779')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Ouverture')
        track.set_length(463)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title("Jillian (I'd Give My Heart)")
        track.set_length(279)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title('The Howling')
        track.set_length(391)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title('Stand My Ground')
        track.set_length(273)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title('The Cross')
        track.set_length(322)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title('What Have You Done?')
        track.set_length(298)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title('Hand of Sorrow')
        track.set_length(340)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title('The Heart of Everything')
        track.set_length(348)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title('Forgiven')
        track.set_length(293)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title('Somewhere')
        track.set_length(264)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('11')
        track.set_title('The Swan Song')
        track.set_length(240)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('12')
        track.set_title('Memories')
        track.set_length(243)
        disc.append_track(track)

        expected.append_disc(disc)

        disc = expected.create_disc()
        disc.set_number(2)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Our Solemn Hour')
        track.set_length(322)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title('The Other Half (of Me)')
        track.set_length(304)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title('Frozen')
        track.set_length(360)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title('The Promise')
        track.set_length(272)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title('Angels')
        track.set_length(495)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title('Mother Earth')
        track.set_length(242)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title('The Truth Beneath the Rose')
        track.set_length(443)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title('Deceiver of Fools')
        track.set_length(458)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title('All I Need')
        track.set_length(295)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title('Ice Queen')
        track.set_length(435)
        disc.append_track(track)

        expected.append_disc(disc)

        s = metalarchives.ReleaseScraper.from_string('http://www.metal-archives.com/albums/Within_Temptation/Black_Symphony/212779')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_multiple_release_artists(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date('April 14th, 2007')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format('Split, 7" vinyl (45 RPM)')

        label_id = expected.create_label_id()
        label_id.set_label('New Iron Age Records')
        label_id.append_catalogue_nr('NIA 002')
        expected.append_label_id(label_id)

        expected.set_title('Clash of Iron Vol. I - Live at Keep It True')

        artist = expected.create_artist()
        artist.set_name('Manilla Road')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        artist = expected.create_artist()
        artist.set_name('Brocas Helm')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.set_url('http://www.metal-archives.com/albums/Manilla_Road/Clash_of_Iron_Vol._I_-_Live_at_Keep_It_True/147439')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Manilla Road - Death by the Hammer')
        track.set_length(None)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title('Brocas Helm - Ravenwreck')
        track.set_length(None)
        disc.append_track(track)

        expected.append_disc(disc)

        s = metalarchives.ReleaseScraper.from_string('http://www.metal-archives.com/albums/Manilla_Road/Clash_of_Iron_Vol._I_-_Live_at_Keep_It_True/147439')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_404(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        s = metalarchives.ReleaseScraper.from_string('http://www.metal-archives.com/albums/Within_Temptation/Black_Symphony/999999999')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_search_scraper(self):
        s = metalarchives.SearchScraper('love')
        r = s.get_result()

        self.assertTrue(len(r.get_items()) > 0)
