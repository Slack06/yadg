#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 tycho
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import TestCase
from ..scraper import deezer
from ..result import NotFoundResult


class DeezerTest(TestCase):

    def test_simple_album(self):
        expected = {
            u'artists': u'Aphex Twin',
            u'title': u'Selected Ambient Works 85-92',
            u'genres': [u'Electro'],
            u'url': u'https://www.deezer.com/en/album/12977824',
            u'labelIds': [{u'catalogueNrs': [], u'label': u'R&S Records'}],
            u'releaseEvents': u'1992-02-10',
            u'discs': [
                [
                    [u'Xtal', 293],
                    [u'Tha', 547],
                    [u'Pulsewidth', 228],
                    [u'Ageispolis', 323],
                    [u'I', 77],
                    [u'Green Calx', 365],
                    [u'Heliosphan', 293],
                    [u'We Are the Music Makers', 463],
                    [u'Schottkey 7th Path', 308],
                    [u'Ptolemy', 434],
                    [u'Hedphelym', 363],
                    [u'Delphium', 329],
                    [u'Actium', 455],
                ]
            ]
        }

        scraper = deezer.Scraper.from_string('https://www.deezer.com/en/album/12977824')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_multiple_cds(self):
        expected = {
            u'artists': u'Autechre',
            u'title': u'EPs 1991 - 2002',
            u'genres': [u'Alternative', u'Electro'],
            u'url': u'https://www.deezer.com/en/album/2796151',
            u'labelIds': [{u'catalogueNrs': [], u'label': u'Warp Records'}],
            u'releaseEvents': u'2011-04-11',
            u'discs': [
                [
                    [u'Cavity Job', 370],
                    [u'Accelera 1 & 2', 399],
                    [u'Basscadet (Bcdtmx)', 408],
                    [u'Basscadet (Basscadoublemx)', 266],
                    [u'Basscadet (Tazmx)', 413],
                    [u'Basscadet (Basscadubmx)', 577],
                    [u'Lost', 444],
                    [u'Djarum', 439],
                    [u'Flutter', 599],
                    [u'Garbagemx', 851],
                    [u'PIOBmx', 457],
                    [u'Bronchusevenmx', 584],
                    [u'VLetrmx', 507],
                    [u'Second Bad Vilbel', 586],
                    [u'Second Scepe', 465],
                    [u'Second Scout', 442],
                    [u'Second Peng', 655],
                    [u'Milk DX', 365],
                    [u'Inhake 2', 516],
                    [u'Drane', 649],
                    [u'Goz Quarter', 582],
                    [u'Latent Quarter', 457],
                    [u'Laughing Quarter', 425],
                    [u'Draun Quarter', 652]
                ],
                [
                    [u'Yeesland', 382],
                    [u'Pencha', 373],
                    [u'Characi', 443],
                    [u'Krib', 191],
                    [u'Tilapia', 374],
                    [u'Rpeg', 360],
                    [u'Ccec', 298],
                    [u'Squeller', 277],
                    [u'Left Blank', 400],
                    [u'Outpt', 432],
                    [u'Dropp', 196],
                    [u'Liccflii', 299],
                    [u'Maphive6.1', 499],
                    [u'Zeiss Contarex', 393],
                    [u'Netlon Sentinel', 246],
                    [u'Pir', 212],
                    [u'Gelk', 530],
                    [u'Blifil', 420],
                    [u'Gaekwad', 385],
                    [u'19 Headaches', 434],
                    [u'Gantz_Graf', 237],
                    [u'Dial.', 377],
                    [u'Cap.IV', 544]
                ]
            ]
        }

        scraper = deezer.Scraper.from_string('https://www.deezer.com/en/album/2796151')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_various_artists(self):
        expected = {
            u'artists': [{u'name': None, u'isVarious': True}],
            u'title': u'Artificial Intelligence',
            u'genres': [u'Alternative', u'Electro'],
            u'url': u'https://www.deezer.com/en/album/2794841',
            u'labelIds': [{u'catalogueNrs': [], u'label': u'Warp Records'}],
            u'releaseEvents': u'1992-07-06',
            u'discs': [
                [
                    [u'The Dice Man', u'Polygon Window', 312],
                    [u'Musicology', u'Telefone 529', 250],
                    [u'Autechre', u'Crystel', 277],
                    [u'I.A.O.', u'The Clan', 307],
                    [u'Speedy J', u'De-Orbit', 373],
                    [u'Musicology', u'Preminition', 244],
                    [u'UP!', u'Spiritual High', 462],
                    [u'Autechre', u'The Egg', 453],
                    [u'Speedy J', u'Fill 3', 223],
                    [u'Dr Alex Paterson', u'Loving You Live', 240]
                ]
            ]
        }

        scraper = deezer.Scraper.from_string('https://www.deezer.com/en/album/2794841')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_404(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        scraper = deezer.Scraper.from_string('https://www.deezer.com/en/album/12')
        result = scraper.get_result()

        self.assertEqual(expected, result)

    def test_search_scraper(self):
        scraper = deezer.SearchScraper('love')
        result = scraper.get_result()

        self.assertTrue(len(result.get_items()) > 0)
